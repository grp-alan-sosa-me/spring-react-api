package net.alansosa.backendcursojava;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*
* This class enables CORS. Here we declare the allowed http methods, headers, and origins that the API
* can exchange data with.
* */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry){
        registry.addMapping("/**") //Declare all endpoints to enable CORS support
                .allowedMethods("*") //All methds incluiding 'OPTIONS'
                .allowedOrigins("*") //origins from the API can communicate with
                .allowedHeaders("*"); //Allow all headers
    }

}
