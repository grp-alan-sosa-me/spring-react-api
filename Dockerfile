#Use openJDk 8 image
FROM openjdk:8-jre-slim
#Create user spring-dev and give access privileges to /bin/bash so it can access by cmd line
RUN useradd -ms /bin/bash spring-dev
#Log in as spring-dev user
USER spring-dev
#Copy the jar from target/ to root folder in the cointaner
ADD ./target/backendcursojava-0.0.1-SNAPSHOT.jar app.jar
#Expose the 8080 port in the container
EXPOSE 8080
#Run the command the run the JVM with our .jar
CMD ["java", "-jar", "app.jar"]
