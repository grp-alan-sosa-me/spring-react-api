# blog-spring-api

This is a SpringBoot RESTful API that implements JWT security, HATEOAS, CORS, Hibernate, JPA and Swagger. 

The project is packaged using Docker. The Image is pushed to Gitlab Container Registry then deployed in Google Cloud Kubernetes Engine.

This API connects to a DB hosted in Amazon RDS and serves as a backend for [this](https://gitlab.com/grp-alan-sosa-me/spring-react-ui) REACT-REDUX application.

## Installation

You can use Maven to build the project.

```bash
mvn package
```

Right after the project is built. The target/ folder will be created containing a jar file.

```bash
java -jar “target/backendcursojava-0.0.1-SNAPSHOT.jar
```

## Usage

You can see the API Documentation [here](http://34.71.79.47:8080/swagger-ui/)

## License
[MIT](https://choosealicense.com/licenses/mit/)