#!/bin/bash

#Get server lists
set -f
server=$DEPLOY_SERVER

ssh ubuntu@$server "cd java/spring-react-api && git pull origin master && mvn install -DskipTests && mv target/backendcursojava-0.0.1-SNAPSHOT.jar target/app.jar && docker-compose up --build -d"